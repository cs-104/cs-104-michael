#!/bin/bash
# AUTHOR: Michael CS-104 Dev minds
# Minimum script to run a report of aws services 
# Simple echo|aws commands for taking service reports 


NOW=$(date +"%m-%d-%Y")

echo ''
echo '******************************************************************************************************'
echo -e "Target service report and Describe AMI $NOW "
echo '******************************************************************************************************'
echo ''
echo 'AWS Sandbox Account accounts:'
  aws ec2 describe-images --region eu-west-1 --filters "Name=name,Values=cs-104-michael-am*" --profile 'sandbox'  --output table

echo 'AWS Preprod Account accounts:'
  aws ec2 describe-images --region eu-west-1 --filters "Name=name,Values=cs-104-michael-am*" --profile 'preprod'  --output table
  
  echo 'AWS Prod Account accounts:'
  aws ec2 describe-images --region eu-west-1 --filters "Name=name,Values=cs-104-michael-am*" --profile 'prod'  --output table