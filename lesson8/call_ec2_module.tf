
provider "aws" {
alias = "sandbox"
region = "eu-west-1"

assume_role {
    role_arn = "arn:aws:iam::984463041714:role/OrganizationAccountAccessRole"
    external_id  = "EXTERNAL_ID"
  }
}

provider "aws" {
alias = "preprod"
region = "eu-west-1"


assume_role {
    role_arn = "arn:aws:iam::023451010066:role/OrganizationAccountAccessRole"
    external_id  = "EXTERNAL_ID"
  }
}

provider "aws" {
alias = "prod"
region = "eu-west-1"


assume_role {
    role_arn = "arn:aws:iam::197064613889:role/OrganizationAccountAccessRole"
    external_id  = "EXTERNAL_ID"
  }
}

module "sandbox" {
    source = "git::https://michaeloshaye@bitbucket.org/cs-104/cs-104-michael.git//lesson8/tf-ec2-module"
	providers =  {
	aws = aws.sandbox

	}
    #bucket name should be unique
    bucket_name = "michael-bucket-cs-104-sandbox"       
}

module "preprod" {
    source = "git::https://michaeloshaye@bitbucket.org/cs-104/cs-104-michael.git//lesson8/tf-ec2-module"
	providers =  {
	aws = aws.preprod

	}
    #bucket name should be unique
    bucket_name = "michael-bucket-cs-104-preprod"       
}

module "prod" {
    source = "git::https://michaeloshaye@bitbucket.org/cs-104/cs-104-michael.git//lesson8/tf-ec2-module"
	providers =  {
	aws = aws.prod
	
	}
    #bucket name should be unique
    bucket_name = "michael-bucket-cs-104-prod"       
}

module "iam_account" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-account"
  version = "~> 3.0"

  account_alias = "awesome-company"

  minimum_password_length = 37
  require_numbers         = false
}
