// Testrepo auth 
provider "aws" {
alias = "sandbox"
region = "eu-west-1"

assume_role {
    role_arn = "arn:aws:iam::984463041714:role/OrganizationAccountAccessRole"
    external_id  = "EXTERNAL_ID"
  }
}

provider "aws" {
alias = "preprod"
region = "eu-west-1"


assume_role {
    role_arn = "arn:aws:iam::023451010066:role/OrganizationAccountAccessRole"
    external_id  = "EXTERNAL_ID"
  }
}

provider "aws" {
alias = "prod"
region = "eu-west-1"


assume_role {
    role_arn = "arn:aws:iam::197064613889:role/OrganizationAccountAccessRole"
    external_id  = "EXTERNAL_ID"
  }
}

module "sandbox_iam_user" {
    source  = "terraform-aws-modules/iam/aws//modules/iam-user"
        providers =  {
        aws = aws.sandbox

        }

  name          = "michael.omega2"
  force_destroy = true

  pgp_key = "keybase:test"

  password_reset_required = false
  
}





module "preprod_iam_user" {
    source  = "terraform-aws-modules/iam/aws//modules/iam-user"
        providers =  {
        aws = aws.preprod

        }

  name          = "michael.omega2"
  force_destroy = true

  pgp_key = "keybase:test"

  password_reset_required = false
  
}

module "prod_iam_user" {
    source  = "terraform-aws-modules/iam/aws//modules/iam-user"
        providers =  {
        aws = aws.prod

        }

 name          = "michael.omega2"
  force_destroy = true

  pgp_key = "keybase:test"

  password_reset_required = false
  
}
