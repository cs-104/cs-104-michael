# Define public subnets		
resource "aws_subnet" "public-subnet" {
 vpc_id      = "${aws_vpc.default.id}"
  cidr_block  = "${var.public_subnet_2a_cidr}"
  availability_zone = "eu-west-1a"
map_public_ip_on_launch = true
tags = {

Name = "Web Public subnet 1"

}

} 	

resource "aws_subnet" "public-subnet2" {
 vpc_id      = "${aws_vpc.default.id}"
  cidr_block  = "${var.public_subnet_2b_cidr}"
  availability_zone = "eu-west-1a"
map_public_ip_on_launch = true
tags = {

Name = "Web Public subnet 2"

}

} 

# Define private subnets	
	
resource "aws_subnet" "private-subnet" {
 vpc_id      = "${aws_vpc.default.id}"
  cidr_block  = "${var.private_db_subnet_2a_cidr}"
  availability_zone = "eu-west-1b"
map_public_ip_on_launch = false
tags = {

Name = "App Private subnet 1"

}

} 	

resource "aws_subnet" "private-subnet2" {
 vpc_id      = "${aws_vpc.default.id}"
  cidr_block  = "${var.private_db_subnet_2b_cidr}"
  availability_zone = "eu-west-1b"
map_public_ip_on_launch = false
tags = {

Name = "App Private subnet 2"

}

} 	

