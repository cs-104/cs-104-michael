target_vpc      = "vpc-0c181c1bf5ac77b42"
target_subnet   = "subnet-099b1eb5924eafe03"
target_keypairs = "EC2 Tutorial"   # alternative to generate a keypair from your machine ssh-keygen -f terraform-keys2
project_name    = "Dev-minds Project VPC "
aws_ami         = "ami-0b850cf02cc00fdc8" # THIS AMI WILL GIVE YOU CENTOS INSTANCE
server_type     = "t2.micro"
