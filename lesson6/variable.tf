provider "aws" {
        region="${var.aws_region}"
		profile="michael-dev-minds"
		
}

variable "region" {
        description="Region Name"
		default="eu-west-1"
		
}



variable "aws_region" {
        description="EC2 Region for the VPC"
		default="eu-west-1"
		
}



variable "vpc_cidr" {
        description="CIDR for the whole VPC"
		default="10.0.0.0/16"
		
}

variable "public_subnet_2a_cidr" {
        description="CIDR for the Public 2a subnet"
		default="10.0.0.0/25"
		
}

variable "public_subnet_2b_cidr" {
        description="CIDR for the Public 2b subnet"
		default="10.0.0.128/25"
		
}

variable "private_db_subnet_2a_cidr" {
        description="CIDR for the Public 2a subnet"
		default="10.0.2.0/25"
		
}	

variable "private_db_subnet_2b_cidr" {
        description="CIDR for the Public 2b subnet"
		default="10.0.2.128/25"
		
}	

variable "key_path" {
        description="SSH Public key path"
		default="C:/Users/admin/.ssh/michael-cs104.pub"
		
}		
	
variable "availability_zones" {
        type=list(string)
		default=["eu-west-1a", "eu-west-1b"]
		
}	
	
variable "aws_ami" {
  default = ""
}

variable "server_type" {
  default = ""
}	

variable "target_vpc" {
  default     = ""
  description = "DevMinds Default VPC: aws acct as default"
}

variable "target_subnet" {
  default     = ""
  description = "DevMinds Default VPC SN: eu-west-1"
}

variable "target_keypairs" {
  default     = ""
  description = "DevMinds default keys:"
}	
	
variable "target_public" {
  default     = ""
  description = "DevMinds default keys:"
}
