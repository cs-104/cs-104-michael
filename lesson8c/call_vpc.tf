#providers
provider "aws" {
    version = ">= 0.12.31"
  profile = "prod"
region = "eu-west-1"
}

 terraform {
  required_version = ">= 0.12.31"

  backend "s3" {
    bucket  = "michael-global-bucket"
    key = "prod/module-concepts/terraform.tfstate"
    region  = "eu-west-1"
   encrypt = "true"
   
  }
}


module "vpc" {

  source        = "git::https://michaeloshaye@bitbucket.org/cs-104/cs-104-michael.git//lesson8c/module_vpc"


}
