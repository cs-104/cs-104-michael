This pipeline Jenkinsfile works based on the choice of users how services should be executed and also if text report file should be generated.

Your jenkins job should use below directives
Parameters: Give users a choice to pick which of the service resource they can run
When: Give users a flexibility to run all service query in one go
BooleanParam: Give users the flexibility to create a txt report on the bastion node or not
Agent: specify label to use the bastion-master-node as the build point

