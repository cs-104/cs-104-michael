#!/bin/bash

#########################################

# Capturing System Information

###################################

# define variable for information
host_name=`hostname -f`
date_report=`date`
sleep 5s

echo ""
echo ""
echo "-----------------------------------------------------------------------------------------------------------------------------------------------------"
echo -e "list resources from below AWS services existing on the centrale acct locally $host_name as at $date_report"
echo "-----------------------------------------------------------------------------------------------------------------------------------------------------"
echo ""
echo ""
echo -e "VOLUMES:\t\t" 

/usr/local/bin/aws ec2 describe-volumes \
    --filters Name=status,Values=available  --output table

echo ""

echo -e "VPCs:\t\t" 

aws ec2 describe-vpcs  --output table

echo ""
echo -e "ORGANIZATION:\t\t" 

/usr/local/bin/aws organizations describe-organization  --output table
echo ""
echo -e "SUBNETS:\t\t" 

/usr/local/bin/aws ec2 describe-subnets  --output table
echo ""
echo -e "IAM USERS:\t\t" 

/usr/local/bin/aws iam list-users --profile default --output table
echo ""
echo -e "S3 BUCKETS:\t\t" 

/usr/local/bin/aws s3 ls --profile default --output table
echo ""
echo -e "EC2:\t\t" 

/usr/local/bin/aws ec2 describe-instances --output table
echo ""

echo -e "ROUTE 53 HOSTED ZONES:\t\t" 

/usr/local/bin/aws route53 list-hosted-zones --output table
echo ""

