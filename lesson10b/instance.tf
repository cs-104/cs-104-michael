# Define our VPC		
resource "aws_vpc" "default" {
  cidr_block  = "${var.vpc_cidr}"

  enable_dns_hostnames = true

tags = {

Name = "Devops POC VPC"

}

}

# Define public subnets		
resource "aws_subnet" "public-subnet" {
 vpc_id      = "${aws_vpc.default.id}"
  cidr_block  = "${var.public_subnet_2a_cidr}"
  availability_zone = "eu-west-1a"
map_public_ip_on_launch = true
tags = {

Name = "Web Public subnet 1"

}

} 	

resource "aws_subnet" "public-subnet2" {
 vpc_id      = "${aws_vpc.default.id}"
  cidr_block  = "${var.public_subnet_2b_cidr}"
  availability_zone = "eu-west-1a"
map_public_ip_on_launch = true
tags = {

Name = "Web Public subnet 2"

}

} 

# Define private subnets	
	
resource "aws_subnet" "private-subnet" {
 vpc_id      = "${aws_vpc.default.id}"
  cidr_block  = "${var.private_db_subnet_2a_cidr}"
  availability_zone = "eu-west-1b"
map_public_ip_on_launch = false
tags = {

Name = "App Private subnet 1"

}

} 	

resource "aws_subnet" "private-subnet2" {
 vpc_id      = "${aws_vpc.default.id}"
  cidr_block  = "${var.private_db_subnet_2b_cidr}"
  availability_zone = "eu-west-1b"
map_public_ip_on_launch = false
tags = {

Name = "App Private subnet 2"

}

} 	

# Create security group for webserver
resource "aws_security_group" "webserver_sg" {
  name  = "sg_ws_name_dev"
 vpc_id      = "${var.target_vpc}"  
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
   }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "Security Group VPC devmind" 
    Project = "demo-assignment"
  }
}


# route tables

resource "aws_route_table" "public-rt" {
 vpc_id      = "${aws_vpc.default.id}"
 route {
 
 cidr_block = "0.0.0.0/0"
 gateway_id = "${aws_internet_gateway.gw.id}"
 
 }
 
 tags = {
    Name = "Public Subnet RT"
 }

}

#assign route table to public subnet
resource "aws_route_table_association" "public-rt1" {
subnet_id = "${aws_subnet.public-subnet.id}"
route_table_id = "${aws_route_table.public-rt.id}"

}
	
#assign route table to public subnet
resource "aws_route_table_association" "public-rt2" {
subnet_id = "${aws_subnet.public-subnet2.id}"
route_table_id = "${aws_route_table.public-rt.id}"

}

# nat gateway

resource "aws_eip" "eip_Nat" {
  vpc = true
 }
resource "aws_nat_gateway" "nat_gw1" {
allocation_id = "${aws_eip.eip_Nat.id}"
subnet_id = "${aws_subnet.public-subnet.id}"
depends_on = ["aws_internet_gateway.gw"]

}
		
#private 

resource "aws_route_table" "private-rt" {
 vpc_id      = "${aws_vpc.default.id}"
 route {
 
 cidr_block = "0.0.0.0/0"
nat_gateway_id = "${aws_nat_gateway.nat_gw1.id}"
 
 }
 
 tags = {
    Name = "Private Subnet RT"
 }

}
	
	#assign route table to private subnet
resource "aws_route_table_association" "private-rt1" {
subnet_id = "${aws_subnet.private-subnet.id}"
route_table_id = "${aws_route_table.private-rt.id}"
}

	#assign route table to private subnet
resource "aws_route_table_association" "private-rt2" {
subnet_id = "${aws_subnet.private-subnet2.id}"
route_table_id = "${aws_route_table.private-rt.id}"
}
	
# Internet Gateway		
resource "aws_internet_gateway" "gw" {
  vpc_id      = "${aws_vpc.default.id}"

tags = {

name = "VPC IGW"

}

}


#make another folder for the ec2
# Make ec2
# Server Definition
resource "aws_instance" "EC2InstanceCreate" {
  ami                    = "${var.aws_ami}"
  instance_type          = "${var.server_type}"
  key_name               = "${var.target_keypairs}"
  subnet_id              = "${var.target_subnet}"
vpc_security_group_ids = [aws_security_group.webserver_sg.id]

  provisioner "remote-exec" {
  connection {
      type    = "ssh"
       host = "${self.public_ip}"
      user    = "centos"
    private_key = "${file("./EC2Tutorial.pem")}"
    timeout     = "5m"
    }

  inline = [
  "sudo yum -y update",
  "sudo yum  -y install epel-release",
  "sudo yum -y install wget, unzip",]


  }
provisioner "local-exec" {
    command = "sleep 30; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u centos -i '${self.public_ip},' --private-key EC2Tutorial.pem provision.yaml"
  }
tags = {
    Name        = "cs-104-lesson10-michael"
    Environment = "DEV"
    App         = "NGINX"
  }  
}

output "pub_ip" {
  value      = ["${aws_instance.EC2InstanceCreate.public_ip}"]
  depends_on = [aws_instance.EC2InstanceCreate]
}
