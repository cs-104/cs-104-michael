resource "aws_s3_bucket" "michael-bucket" {
   
    bucket = "${var.bucket_name}" 
    acl = "${var.acl_value}"   
}
