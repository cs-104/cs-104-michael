provider "aws" {
region = "eu-west-1"
profile ="${terraform.workspace}"
}
module "s3" {
    source = "git::https://michaeloshaye@bitbucket.org/cs-104/cs-104-michael.git//lesson8-jenkins/s3bucket/tf-s3-module-jenkins"
 #bucket name should be unique
    bucket_name = "bucket-name-${terraform.workspace}-test-now-august"
}
