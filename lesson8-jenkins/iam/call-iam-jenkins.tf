provider "aws" {
region = "eu-west-1"
profile ="${terraform.workspace}"
}
module "iam" {
    source = "git::https://michaeloshaye@bitbucket.org/cs-104/cs-104-michael.git//lesson8-jenkins/iam/iam-users"
    iam-username = "mike-test-2"
    bucket_name = "michael-iam-${terraform.workspace}"
	tag-user =  "michael-iam-${terraform.workspace}"
}
